<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', 'Auth\AuthController@Register');
Route::post('login', 'Auth\AuthController@Login');
Route::get('logout', 'Auth\AuthController@Logout');
Route::get('user', 'Auth\AuthController@GetAuthUser');
Route::get('course', 'Auth\AuthController@Courses');
Route::post('course', 'Auth\AuthController@CoursesRegister');
Route::get('print', 'Auth\AuthController@MaatexcelCourses');
Route::get('courses', 'Auth\AuthController@processQueue');
