<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Jobs\CourseCreate;
use App\Course;
use Faker\Generator as Faker;
use App\Exports\ExcelExport;
use Maatwebsite\Excel\Facades\Excel;

class AuthController extends Controller 
{
    //to login after registration
    public $loginAfterSignUp = true;


    //registration endpoint 
    //requires username, email and password
    //and will lead to login request
    public function Register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'email' => 'required|string|email|max:100|unique:users',
                'password' => 'required|string|min:8|confirmed',
            ]);
    
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
                
    
            $token = auth()->login($user);
            return ($user)? response()->json([
                'message' => 'success',
            'data' => $user,
            'token' => $token], 200)
             : $this->forbidden($user);
    
        } catch (\Exception $th) {
            return response()->json('Error: '.$th->getMessage(), 400);
        }
       
    }


    //endpoint to login
    //requires email and password
    public function Login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);


        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth()->user();
        
        return response()->json([
            'message' => 'success',
            'data' => $user,
            'access_token' => $token,
        ], 200);;
    }

    //get jwt request
    public function GetAuthUser(Request $request)
    {
        try {

            if (! $user = auth()->user()) {
                return response()->json(['user not found'], 404);
            }
            return response()->json(auth()->user());

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    //endpoint to logout
    public function Logout()
    {
        try {
            auth()->logout();
        return response()->json(['message'=>'Successfully logged out']);
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return  response()->json(['Could not create token!'], $e->getStatusCode());
        }
        
    }

     //failed response
    //jwt response function
    protected function failedMessage($token)
    {
        return response()->json([
            'message' => 'failed',
        ], 400);
    }

    //endpoint to seed 50 courses using queue
    public function processQueue()
    {
        $create = new CourseCreate;
        dispatch($create);
        return response()->json('success', 200);
    }

    //endpoint to show all courses + regd ones
    public function Courses(Request $request)
    {
        
        $course = Course::all();
        $authUser = auth()->user();
        if(!$authUser['id'])
            return response()->json($course, 200);   
        
        try {
            $id = $authUser['id'];
            $registered = User::find($id)->courses()->get();
            $courses = [];
            foreach ($course as $key => $value) {           
                for ($i=0; $i < count($registered); $i++) { 
                    if($value['id'] == $registered[$i]['pivot']['course_id']){
                        $value = $registered[$i];
                    }
                }
                $courses[] = $value;
        }
        return response()->json($courses, 200);
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        
        
    }

    //endpoint to register an array of courses
    public function CoursesRegister(Request $request)
    {

        try {

            if (! $authUser = auth()->user()) {
                return response()->json(['user not found'], 404);
            }

            $id = $authUser['id'];
            $user = User::find($id);
            $course_id_array = $request->input('courses');
           
            $attach = $user->courses()->syncWithoutDetaching($request->input('courses'));    
            return response()->json('success', 200);
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    //endpoint to export courses using Maatexcel
    public function MaatexcelCourses()
    {
        return Excel::download(new ExcelExport, 'courses.xlsx');
    }
}
