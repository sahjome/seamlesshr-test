<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    protected $fillable = [
        'text','title','created_at', 'updated_at','instructor', 'category',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'course_registration')->withTimestamps();
    }
}
