<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [

        'text' => $faker->text($maxNbChars = 30),
        'title' => $faker->word(),
        'instructor' => $faker->name,
        'category' => $faker->bs,
        'updated_at' => now(),
        'created_at' => now(),
    ];
});
